﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paylike.NET.Constants
{
		public enum ResponseCode
		{
			UnknownResponseCode = 0,
			GetOk = 200,
			PostOk = 201,
			PutOrDeleteOk = 204,
			InputError = 400,
			UnauthorizedAPIKey = 401,
			Forbidden = 403,
			Conflict = 409,
			ServerError = 500
		}
	}
