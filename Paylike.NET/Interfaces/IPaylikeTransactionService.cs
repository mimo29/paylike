﻿using Paylike.NET.Entities;
using Paylike.NET.RequestModels.Transactions;
using Paylike.NET.RequestModels.Payments;
using Paylike.NET.ResponseModels;
using Paylike.NET.ResponseModels.Transactions;
using Paylike.NET.ResponseModels.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paylike.NET.Interfaces
{
    public interface IPaylikeTransactionService
    {
        ApiResponse<CreateTransactionResponse> CreateTransaction(CreateTransactionRequest request);

        ApiResponse<Transaction> CaptureTransaction(CaptureTransactionRequest request);

        ApiResponse<Transaction> RefundTransaction(RefundTransactionRequest request);

        ApiResponse<Transaction> VoidTransaction(VoidTransactionRequest request);

        ApiResponse<Transaction> GetTransaction(GetTransactionRequest request);

        ApiResponse<List<Transaction>> GetTransactions(GetTransactionsRequest request);

        ApiResponse<CreatePaymentResponse> CreatePayment(CreatePaymentRequest request);
    }
}
