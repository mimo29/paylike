﻿using Newtonsoft.Json;
using Paylike.NET.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paylike.NET.RequestModels.Payments
{
    public class CreatePaymentRequest : RequestBase
    {
        public CreatePaymentRequest()
        {
            base.UriTemplate = "/transactions";
            base.Name = "CreatePayment";
            base.HttpMethod = System.Net.WebRequestMethods.Http.Post;
            base.Uri = string.Format("/transactions");
        }

        [JsonIgnore]
        private string merchantId;

        [JsonIgnore]
        public string MerchantId
        {
            get
            {
                return merchantId;
            }
            set
            {
                merchantId = value;
                base.Uri = string.Format(base.UriTemplate, merchantId);
            }
        }
        [JsonProperty("key")]
        public string PublicKey { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("card")]
        public CreditCard Card { get; set; }
    }
}

