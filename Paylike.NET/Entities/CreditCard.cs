﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paylike.NET.Entities
{
    public class CreditCard
    {
        [JsonProperty("number")]
        public string CardNumber { get; set; }

        [JsonProperty("expiry")]
        public CreditCardExpiry CardExpiry { get; set; }

        [JsonProperty("code")]
        public string CardCode { get; set; }
    }
}
